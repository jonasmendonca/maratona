#include <bits/stdc++.h>

using namespace std;

int main(){
	int alunos, vagas;
	int n;
	int ans = 0;
	map<int, int> competidores;
	
	cin>>alunos>>vagas;
	
	for(int x=0; x<alunos; x++){
		cin>>n;
		competidores[n]++;	
	}
	
	map<int, int>::iterator it;
	bool flag = false;
	it = competidores.end();
	it--;
	for(; flag == false; it--){
		if(it == competidores.begin()) flag = true;
		if(vagas>0){
			ans += it->second;
			vagas-= it->second;
		}else{
			break;
		}
		//cout << "Pontuação: " << it->first << endl;
		//cout << "Qtd: " << it->second << endl;
	}
	
	cout << ans << endl;
	
	return 0;
}